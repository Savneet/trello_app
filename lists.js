let APIKey='21ae987b7a73ece5b631ea712594c3ba';
let APIToken='ATTAa33db1ff5166bc555197f0bf97394f4b4fae3d92498b80b1e3ed0da31d918919BF9E6444';

window.onload = function () {
    const params = new URLSearchParams(window.location.search);
    const boardId = params.get("boardId");
    const boardName=params.get("boardName");
    console.log(boardName);
    if (boardId) {
        fetchLists(boardId,boardName);
    }
    console.log(boardId);

    const modal = document.getElementById('my_modal_1');
    const newListForm = document.getElementById('new-list-form');
    const closeModalButton = document.getElementById('close-modal'); 
    newListForm.addEventListener('submit', (event) => {
      event.preventDefault();
      const listName = document.getElementById('new-list-name').value;
      if (listName !== '') {
          createNewList(boardId,listName);
          modal.close();
      }
    });

    closeModalButton.addEventListener('click', (event) => {
      event.preventDefault();
      modal.close();
    });

    
};

function fetchLists(boardId,boardName) {
fetch(
    `https://api.trello.com/1/boards/${boardId}/lists?key=${APIKey}&token=${APIToken}`,
    {
    method: "GET",
    }
)
    .then((response) => {
    console.log(`Response: ${response.status} ${response.statusText}`);
    return response.json();
    })
    .then((data) => {
    console.log(data);
    displayLists(data,boardName);
    })
    .catch((error) => console.error(error));
}

function displayEachList(listData)
{
    let listsContainer = document.getElementById('listsContainer');
    let newList = document.createElement('div');
    newList.textContent=listData.name;
    // newList.textContent.classList.add('pb-10');
    newList.classList.add('bg-slate-300' , 'rounded-md','font-bold','pt-2','pl-4','h-24');
    newList.id=listData.id;
    console.log(newList);
    listsContainer.appendChild(newList);
    let deleteButton = document.createElement('button');
    deleteButton.classList.add('float-end','mr-2', 'bg-slate-500','text-white', 'p-2', 'rounded-md');
    deleteButton.textContent ='Delete List';
    newList.appendChild(deleteButton);
    deleteButton.addEventListener('click',()=>{
    deleteList(listData)
   });
   let cardContainer = document.createElement('div');
    cardContainer.id='card-container';
    cardContainer.classList.add('mt-8');
    newList.appendChild(cardContainer);
   fetchCards(listData);
   let addNewCardBtn = document.createElement('button');
   addNewCardBtn.textContent='+ Add a card';
   addNewCardBtn.classList.add('pb-4','pl-2','outline-none')
   addNewCardBtn.addEventListener('click',()=>{
    openModal(listData)
   })
   newList.appendChild(addNewCardBtn);
}

function displayLists(listsData,boardName)
{
    console.log(listsData);
    let boardHeading= document.getElementById('boardHeading');
    let boardNameHeading = document.createElement('div');
    boardNameHeading.textContent=boardName;
    boardHeading.appendChild(boardNameHeading);
    // let listsContainer = document.getElementById('listsContainer');
    listsData.forEach((list)=>{
        displayEachList(list);
    });

}

function displayNewList(listData)
{
    displayEachList(listData);
}

function createNewList(boardId,listName)
{
    // let listName = prompt('Enter the list name');
    if(listName!=null)
    {
        fetch(`https://api.trello.com/1/lists?name=${listName}&idBoard=${boardId}&key=${APIKey}&token=${APIToken}`, {
        method: 'POST'
        })
        .then(response => {
            console.log(
            `Response: ${response.status} ${response.statusText}`
            );
            return response.json();
        })
        .then(data =>{
            console.log(data);
            displayNewList(data);
        } )
        .catch(err => console.error(err));
    }
}

function deleteList(listData)
{
    let id=listData.id;
    fetch(`https://api.trello.com/1/lists/${id}/closed?key=${APIKey}&token=${APIToken}&value=true`, {
    method: 'PUT'
    })
    .then(response => {
        console.log(
        `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
    })
    .then((data)=> {
        removeDeletedList(data);
            
    })
    .catch(err => console.error(err));
}

function removeDeletedList(deletedListData)
{
    console.log(deletedListData);
    let elementToBeDeleted=document.getElementById(deletedListData.id);
    elementToBeDeleted.remove();
}

function fetchCards(listData)
{
    let id=listData.id;
    fetch(`https://api.trello.com/1/lists/${id}/cards?key=${APIKey}&token=${APIToken}`, {
    method: 'GET',
    headers: {
        'Accept': 'application/json'
    }
    })
  .then(response => {
    console.log(
      `Response: ${response.status} ${response.statusText}`
    );
    return response.json();
  })
  .then(data => {
    displayCards(data);
  })
  .catch(err => console.error(err));
}

function displayCards(cardDataOfEachList)
{
    console.log(cardDataOfEachList);
    cardDataOfEachList.forEach((eachCard)=>
    {
        displayNewCard(eachCard);
    });
}

function displayNewCard(cardData)
{
        let listForTheCards=document.getElementById(cardData.idList);
        listForTheCards.classList.remove('h-24');
        console.log(listForTheCards);
        let cardContainer = listForTheCards.querySelector('#card-container');
        let newCard=document.createElement('div');
        newCard.id=cardData.id;
        newCard.textContent=cardData.name;
        newCard.classList.add('text-black','rounded-md', 'relative','p-2', 'border', 'font-normal', 'my-4','mr-4','bg-white','flex','justify-between');
        let deleteCardIcon= document.createElement('i');
        deleteCardIcon.classList.add('fa-solid','fa-xmark');
        deleteCardIcon.classList.add('mr-2','pt-1');
        deleteCardIcon.addEventListener('click',()=>{
            deleteCard(cardData);
        })
        cardContainer.appendChild(newCard);
        newCard.appendChild(deleteCardIcon);
}

function createNewCard(listData, cardName)
{
    let id = listData.id;
    fetch(`https://api.trello.com/1/cards?name=${cardName}&idList=${id}&key=${APIKey}&token=${APIToken}`, {
    method: 'POST',
    headers: {
        'Accept': 'application/json'
    }
    })
    .then(response => {
        console.log(
        `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
    })
    .then(data => displayNewCard(data))
    .catch(err => console.error(err));
}

function openModal(listData)
{
    const modal = document.getElementById('card-modal');
    const newCardForm = document.getElementById('new-card-form');
    const closeModalButton = document.getElementById('close-card-modal'); 
    newCardForm.addEventListener('submit', (event) => {
      event.preventDefault();
      const cardName = document.getElementById('new-card-name').value;
      if (cardName !== '') {
          createNewCard(listData,cardName);
          modal.close();
      }
    });

    closeModalButton.addEventListener('click', (event) => {
      event.preventDefault();
      modal.close();
    });
    modal.showModal();
}

function deleteCard(cardData)
{
    let id=cardData.id;
    fetch(`https://api.trello.com/1/cards/${id}?key=${APIKey}&token=${APIToken}`, {
    method: 'DELETE'
    })
    .then(response => {
        console.log(
        `Response: ${response.status} ${response.statusText}`
        );
        return response.json();
    })
    .then(() => removeDeletedCard(cardData))
    .catch(err => console.error(err));
}

function removeDeletedCard(deletedCardData)
{
    console.log(deletedCardData);
    let elementToBeDeleted=document.getElementById(deletedCardData.id);
    elementToBeDeleted.remove();
}




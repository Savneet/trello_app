let APIKey='21ae987b7a73ece5b631ea712594c3ba';
let APIToken='ATTAa33db1ff5166bc555197f0bf97394f4b4fae3d92498b80b1e3ed0da31d918919BF9E6444';

function getBoards(){ 
    fetch(`https://api.trello.com/1/members/me/boards?key=${APIKey}&token=${APIToken}`, {
    method: 'GET',
    })
    .then(response => 
        {
            console.log(`Response: ${response.status} ${response.statusText}`);
            return response.json();
        })
    .then(boards => displayBoards(boards))
    .catch(err => console.error(err));
}

function displayEachBoard(boardData)
{
  let boardContainer=document.getElementById('board-container');
  let boardElement = document.createElement('a');
  boardElement.classList.add('bg-sky-700', 'h-20', 'text-slate-50', 'rounded','font-bold','board','pt-2','pl-4');
  boardElement.textContent=boardData.name;
  boardElement.id=boardData.id;
  boardElement.target='_blank';
  boardElement.href=`board.html?boardId=${boardElement.id}&boardName=${boardElement.textContent}`;
  boardContainer.appendChild(boardElement);
}

function displayBoards(boards){
    
    let filterBoards=boards.filter((board)=>board.name!=='Kanban Template');
    filterBoards.forEach((board)=>{
      displayEachBoard(board);
    })
    
}

function displayParticularBoard(boardData)
{
  displayEachBoard(boardData);
}

function createNewBoard(boardName) { 
    if(boardName!==null)
    {
      fetch(
        `https://api.trello.com/1/boards/?name=${boardName}&key=${APIKey}&token=${APIToken}`,
        {
        method: "POST",
        }
      )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.json();
      })
      .then((data)=> displayParticularBoard(data))
      .catch((err) => console.error(err));
    }
}

window.onload=()=>
{
    getBoards();
    const modal = document.getElementById('my_modal_1');
    const newBoardForm = document.getElementById('new-board-form');
    const closeModalButton = document.getElementById('close-modal'); 
    newBoardForm.addEventListener('submit', (event) => {
      event.preventDefault();
      const boardName = document.getElementById('new-board-name').value.trim();
      if (boardName !== '') {
          createNewBoard(boardName);
          modal.close();
      }
    });

    closeModalButton.addEventListener('click', (event) => {
      event.preventDefault();
      modal.close();
    });
}





